// Ejercicio 1
// Dada una matriz de N elementos en la que todos los elementos son iguales excepto uno,
// crea una función findUniq que retorne el elemento único.

function findUniq(array) {
    let uniqueElement;
    array.forEach(function (elemento, indice, array) {
        let popElemento = array.pop();
        if (!array.includes(popElemento)) {
            uniqueElement = popElemento;
        }
    })

    console.log(uniqueElement);
    return uniqueElement;
}


/**
 * TEST Ejercicio 1
 */
findUniq(['12', 10, '12', 11, 1, 11, 10, '12']) // 1
findUniq(['Capitán América', 'Hulk', 'Deadpool', 'Capitán América', 'Hulk', 'Wonder Woman', 'Deadpool', 'Iron Man', 'Iron Man']) // 'Wonder Woman'
