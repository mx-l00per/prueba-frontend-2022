// Ejercicio 2
// Dada una matriz de N elementos repetidos,
// crea una función numbersTop para obtener los tres elementos más repetidos ordenados de forma descendente por número de repeticiones.

function numbersTop(array) {
    let arrayCount = array.reduce((prev, cur) => ((prev[cur] = prev[cur] + 1 || 1), prev), {});
    let firstKey = '0', secondKey = '0', thirdKey = '0';
    let first = 0, second = 0, third = 0;

    Object.entries(arrayCount).forEach(([key, value]) => {
        if (first < value) {
            third = second;
            thirdKey = secondKey;
            second = first;
            secondKey = firstKey;
            first = value;
            firstKey = key;
        }else if(second < value) {
            third = second;
            thirdKey = secondKey;
            second = value;
            secondKey = key;
        }else if(third < value){
            third = value;
            thirdKey = key;
        }
    });

    console.log([firstKey, secondKey, thirdKey]);
    return [firstKey, secondKey, thirdKey];
}

/**
 * TEST Ejercicio 2
 */
numbersTop([3, 3, 1, 4, 1, 3, 1, 1, 2, 2, 2, 3, 1, 3, 4, 1]) // [ 1, 3, 2 ]
numbersTop(['a', 3, 2, 'a', 2, 3, 'a', 3, 4, 'a', 'a', 1, 'a', 2, 'a', 3]) // [ 'a', 3, 2 ]
